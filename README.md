
# An interactive demo of the ND Safir software

This small project is a simple illustration of using a jupyter notebook along with the AllGo SaaS platform (as a backend for image processing computations). Several improvements may be envisaged to make it even easier for end users.

This interactive demo may be used in conjunction with other approaches (like Galaxy). Note that they may be immediately extended to image processing **workflows** (being based on the AllGo REST API).


## 1. Create an account on the Inria AllGo SaaS platform

AllGo is a SaaS (Software as a Service) platform provided by Inria. It may be seen as a virtual showroom of technonogies developed by research teams.

First follow https://allgo.inria.fr/users/sign_up to create an account on AllGo (anyone may create such an account). Once your account creation is confirmed, please connect to https://allgo.inria.fr to obtain your private token, which will allow yo to use the AllGo REST API. You will need this token later (cf. §3 below).


## 2. Install Docker on your computer

Docker is an convenient way to very simply start a notebook server on your local machine. This will change later, once we will also provide a jupyter server (along with AllGo).

If Docker is not already installed on your computer, you should preferably refer to https://docs.docker.com/install.

On a macOS machine, follow these indications: https://docs.docker.com/docker-for-mac/install/

## 3. Download the ndsafir-demo project, and launch the jupyter Notebook

You then need to clone the Git project, for instance with the following command line:
```
git clone https://gitlab.inria.fr/cdeltel/ndsafir-demo.git
```
Create a environement variable ALLGO_TOKEN


Then, launch the following command in a Terminal session to start the notebook server:
```ruby
docker run -v ${PWD}:/home/jovyan  -it --rm -p 8888:8888 jupyter/scipy-notebook
```

Look at the command output. You will find something like:

```
Copy/paste this URL into your browser when you connect for the first time,
to login with a token:
   http://localhost:8888/?token=xxxxxxxxxxxxx_private_token_xxxxxxxxxxxxx

```

Open the indicated **local http address** on your web browser. This brings you to the **jupyter notebook**
 dashboard, where you will navigate to find the notebook: click on the **ndsafir-demo-notebook.ipynb** file, which will open the ndsafir demo notebook.

If this is the first time you've played with a jupyter notebook, basic commands may be found here : https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html#executing-a-notebook

**Quickstart**: The simplest way is to run the notebook step-by-step (one "cell" a time) by pressing shift + enter, once you have selected a cell. Then go to the next cell thanks to the up/down arrows on your keyboard.

## 4. What does it look like?

### 4.a Interactive mode

![](images/illustration_interactif.png)

### 4.b Parameter screening mode

![](images/illustration_balayage_1.png)

![](images/illustration_balayage_2.png)

Authors : Charles Deltel, Sebastien Campion